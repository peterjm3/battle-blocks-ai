from Bot.Strategies.AbstractStrategy import AbstractStrategy
from Bot.Game.Field import Field
import copy


class BestScoreStrategy(AbstractStrategy):

    def __init__(self, game):
        AbstractStrategy.__init__(self, game)
        self._actions = ['left', 'right', 'turnleft', 'turnright', 'down', 'drop']
        self._weights = (-300, 700, -30, -75)

    def choose(self):
        field = self._game.me.field
        piece = self._game.piece
        moves = []
        
        bestRotate = 0
        bestColumn = 0
        bestScore = -100000

        columnHeights = field.heights

        for column in range(-3,10):

          piece._rotateIndex = 0
          for rIndex in range(0,len(piece._rotations)):
            newField, depth = field.projectPieceDown(piece, [column, 0])
            if newField == None:
              continue 
            score = self.scoreMove(newField, piece, [column, depth])
            if score > bestScore:
              bestScore = score
              bestRotate = rIndex
              bestColumn = column
            piece.turnRight()
        
        for x in range(0,bestRotate):
          moves.append('turnright')
        

        for slides in range(0, abs(self._game.piecePosition[0] - bestColumn)):
          if (self._game.piecePosition[0] > bestColumn):
            moves.append('left')
          else:
            moves.append('right')
        moves.append('drop')
        return moves
     
    def scoreMove(self, field, piece, offset):
      heights = copy.deepcopy(self._game.me.field.heights)
      addedHoles = 0
      completedLines = 0

      pos = Field._Field__offsetPiece(piece.positions(), offset)
      for x,y in pos:
        if y>0 and field[y-1][x] == 0:
          heights[x] = 20-y
        if ((x<9 and field[y][x+1] != 4) or (x==9)) and 0 not in field[y]:
          completedLines = completedLines + 1
        holeCounter = 1
        while holeCounter+y < 20 and field[y+holeCounter][x] == 0:
          addedHoles = addedHoles + 1
          holeCounter = holeCounter + 1
      mountains = 0
      for column in range(0,9):
        mountains = mountains + abs(heights[column] - heights[column+1])
      avgHeight = sum(heights)/len(heights)
            
      return (self._weights[0] * avgHeight) + (self._weights[1] * completedLines) + (self._weights[2] * addedHoles) + (self._weights[3] * mountains)

    def scoreField(self, field):
        heights= [0]*10
        completeLines = 0
        holes = 0
        solids = (self._game.round/15)

        #calculate the height for each column
        for x in range(0,10):
            d = 0
            while d<20 and field[d][x] == 0:
                d=d+1
            heights[x] = 20-solids-d
            d= d+1
            while d<(20-solids) and field[d][x] == 0:
                holes = holes + 1
                d=d+1

        tallest = max(heights)

        for line in range(20-solids-tallest,20-solids):
          if 0 not in field[line]:
            completeLines = completeLines + 1
        
        mountains = 0
        for columns in range(0,9):
          mountains = mountains + abs(heights[columns] - heights[columns + 1])

        averageHeight = sum(heights) / 10

        return (-.51 * averageHeight) + (.76 * completeLines) + (-.36 * holes) + (-.18 * mountains)

