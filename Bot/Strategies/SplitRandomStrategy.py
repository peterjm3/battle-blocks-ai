from Bot.Strategies.AbstractStrategy import AbstractStrategy


class SplitRandomStrategy(AbstractStrategy):
    def __init__(self, game):
        AbstractStrategy.__init__(self, game)
        self._actions = ['left', 'right', 'turnleft', 'turnright', 'down', 'drop']

    def choose(self):

     #   target = open("output.txt", 'a')
      #  target.close()
        field = self._game.me.field.field
        w = self._game.me.field.width
        h = self._game.me.field.height

        depths = [0]*w

        for x in range(0,w):
            d = 0
            while d<h and (field[d][x] == 0 or field[d][x] == 1):
                d=d+1
            depths[x] = d

        smallest = depths.index(max(depths))
        dist = self._game.piecePosition[0] - smallest

        moves = []



        for move in range(0,abs(dist)):
            if (dist<0):
                moves.append('right')
            else:
                moves.append('left')



        moves.append('drop')

        return moves
