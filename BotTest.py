from Bot import Planner
from Bot.Game.Game import Game
from Bot.Game import Piece
from Bot.Game.Field import Field
from pyevolve import G1DList
from pyevolve import GSimpleGA
import random
def eval_func(chromosome):
  game = Game()
  planner = Planner.create("score", game)
  planner._strategy._weights = tuple(chromosome)

  score = 0.0

  game.me.field.updateField([[0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0]])
  pieces = ['L', 'O', 'I', 'J', 'S', 'T', 'Z']
  counter = 0
  while (sum(game.me.field.field[15]) == 0 and counter < 100):
    piece = Piece.create(random.choice(pieces))
    game.piece = piece
    game.piecePosition = (0,0)
    offset = (0,0)
    moves = planner.makeMove()
    piece._rotateIndex = 0
    for step in range(0, len(moves)):
      if moves[step] == planner._strategy._actions[0]:
        offset = (offset[0] - 1, offset[1])
        
      if moves[step] == planner._strategy._actions[1]:
        offset = (offset[0] + 1, offset[1])
      if moves[step] == planner._strategy._actions[2]:
        piece.turnLeft()
      if moves[step] == planner._strategy._actions[3]:
        piece.turnRight()
      if moves[step] == planner._strategy._actions[4]:
        break
    depth = 0
    field, depth = game.me.field.projectPieceDown(piece, offset) 

    y = depth
    while y < 20 and y - depth < 5:
      if 0 not in field[y]:
        field.pop(y)
        score += 1
        field.insert(0, [0,0,0,0,0,0,0,0,0,0])
      else:
        for x in range(0,10):
          field[y][x] = 2
      y += 1
    game.me.field.updateField(field)
    counter += 1

  print(counter)
  return counter

class Test:
  def __init__(self, strategy):
    self.game = Game()
    self._planner = Planner.create(strategy, self.game)
  '''
  def run(self):
    self.game.piece = Piece.create('T')
    self.game.piecePosition = (0, 0)
    self.game.me.field.updateField(
                         [[0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0,0],
                          [2,2,2,0,0,0,2,2,2,2],
                          [2,2,2,2,0,2,2,2,2,2]])
    print("Calculated next move:\n")
    print(self._planner.makeMove())
  '''
  def evolveParams(self):
    genome = G1DList.G1DList(4)
    genome.evaluator.set(eval_func)
    genome.setParams(rangemin=-100, rangemax=100)
    ga = GSimpleGA.GSimpleGA(genome)
    ga.evolve(freq_stats=1)

    print ga.bestIndividual()
  
if __name__ == '__main__':
  Test("score").evolveParams()
